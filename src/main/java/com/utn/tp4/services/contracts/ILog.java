package com.utn.tp4.services.contracts;

import com.utn.tp4.models.Log;

import java.util.List;
import java.util.Optional;

public interface ILog {

    Log save(Log data);
    List<Log> getAll();
    Optional<Log> getLogById(long id);
}

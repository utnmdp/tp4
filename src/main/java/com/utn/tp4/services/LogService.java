package com.utn.tp4.services;

import com.utn.tp4.models.Log;
import com.utn.tp4.services.contracts.ILog;
import com.utn.tp4.repositories.LogRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Service
public class LogService implements ILog {

    @Autowired
    private LogRepository logRepository;

    @Override
    public Optional<Log> getLogById(long id) {
        return logRepository.findById(id);
    }

    @Override
    public List<Log> getAll() {
        return logRepository.findAll();
    }

    @Override
    public Log save(Log log) {
        return logRepository.save(log);
    }

}

# Laboratorio V

##Trabajo practico N°4: API REST Spring

###Explique los siguientes goals:
1. mvn clean: limpia los artefactos creados por builds anteriores.
2. mvn compile: Compila el codigo fuente del proyecto.
3. mvn package: Empaqueta el codigo compilado previamente a su formato distribuible, por ejemplo JAR, WAR, etc.
4. mvn install: Instala el/los paquetes en el repositorio local para usarlos como dependencias en otros proyectos.

Fuente: [Maven in 5 Minutes](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)

###Explique los siguientes scopes:
1. compile: Es el scope que se utiliza por defecto. Indica que la dependencia es necesaria para compilar. La dependencia ademas se propaga en los proyectos dependientes.
2. provided: Es como la anterior, pero se espera que el contenedor ya tenga esa libreria. Un claro ejemplo es cuando desplegamos en un servidor de aplicaciones, que por defecto, tiene bastantes librerias, asi que no necesitamos desplegar la dependencia.
3. runtime: La dependencia es necesaria en tiempo de ejecucion pero no es necesaria para compilar.
4. test: La dependencia es solo para testing. JUnit es un claro ejemplo de esto.
5. system: Es como provided, solo que se debe incluir la dependencia explicitamente.
6. import: Este solo se usa en la seccion dependencyManagement. Indica la dependencia a reemplazar en el listado de dependencias de la seccion dependencyManagement.

Fuente: [Introduction to the Dependency Mechanism](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)

###Que es un Arquetype?
Un arquetype es una plantilla o molde. Cuando se crea un proyecto de tipo maven se debe indicar que arquetipo usar.
Esta plantilla crea la estructura basica del proyecto. el contenido del archivo pom.xml, etc.

###Defina la estructura base de un proyecto Maven:
1. src/main/java: Es donde se guardaran las clases se se vayan creando para el proyecto. Dentro de este directorio se
pueden crear distintos paquetes para organizar mejor la estructura de clases.
2. src/main/resources: En esta carpeta se pueden guardar recursos/assets como imagenes, archivos "properties" que se
pueden necesitar a lo largo del proyecto. Tambien es donde van los archivos de configuracion de Spring y Hibernate.
3. src/test/java: En dicha carpeta se guardan las clases de test que se encargarán de probar el correcto funcionamiento
del proyecto.

###Defina la diferencia entre Arquetype y Artifact:
Un artefacto es un archivo, por lo general con extension "JAR" que se implementa en un repositorio maven.
Un build en maven produce uno o mas artefactos. Cada artefacto tiene un groupId (como com.osqui.pro), un artifactId (que seria el nombre) y una version.
Estos tres elementos identifican al artefacto de forma unica. Las dependencias de los proyectos se especifican como artefactos.
Por otro lado un arquetype es una plantilla, no una dependencia, determina la estructura base del proyecto.

###Explique los 4 stereotypes basicos y realice un diagrama de cada uno:
1. Component: es el estereotipo principal, indica que la clase anotada es un component. "@Repository", "@Service" y "@Controller" son especializaciones de "@Component".
2. Controller: Indica que esa clase es un controlador de "recursos". Es el encargado de llamar al resto de componentes para que realicen alguna accion. Tambien se encarga de parsear el request y response a distintos formatos.
2. Service: Indica que la clase es un bean de la capa de negocio.
4. Repository: Es la implementacion del patron "Repository". Son los mediadores entre los objetos de dominio de la aplicacion y el sistema de persistencia utilizado.

### Explique cada uno de los verbos REST:
HTTP (protocolo de transferencia de hipertexto) es el protocolo de comunicación que permite las transferencias de información en la www (World Wide Web).
Este protocolo define, entre otras tantas cosas, una serie de metodos de comunicacion, o tambien conocidos como "verbos HTTP". Los verbos HTTP mas utilizados son:

1. GET: Pide una representación del recurso especificado. En otras palabras es el que se deberia utilizar para pedir informacion de "algo".
2. PUT: Este metodo se utiliza para actualizar los datos de un recurso.. teoricamente PUT se deberia de utilizar para actualizar todos los datos del recurso.
3. POST: Envia datos para que sean procesados. Los datos NO se envian por la URL (como con el metodo GET) sino que se envian en el "cuerpo" de la peticion.
Hay gente que lo considera mas "seguro" solo porque los datos no se ven en la URL... pero es completamente falso.
4. DELETE: Borra el recurso especificado.
5. PATCH: Cumple la misma funcion que PUT, solo que se utiliza para actualizar ciertos datos del recurso, no todos.
6. OPTIONS: Este devuelve los metodos HTTP que el servidor soporta para un url en concreto. Este metodo se utiliza mucho cuando se usan framework frontend como Angular, React, etc.

NOTA: Hay muchos mas verbos HTTP (20 en total), pero estos que enumere considero que son los mas importantes.

Tecnicamente se podria utilizar el verbo POST para todo, es decir, pedir datos, guardar datos, actualizarlos, etc.. pero cuando
se desarollan APIs de tipo REST se debe prestar especial atencion a que tipo de verbo usar para cada cosa.. cuando respetamos
todas las recomendaciones que nos da REST se dice que estamos haciendo una API REST Full. Menciono esto ya que REST se basa
completamente en el protocolo HTTP (De hecho la persona que creo "REST" fue uno de los que participo en la especificacion del protocolo HTTP. Soy un nerd lose.)
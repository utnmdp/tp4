package com.utn.tp4.controllers;

import com.utn.tp4.models.Log;
import com.utn.tp4.services.LogService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/logs")
public class LogController {

    @Autowired
    private LogService logService;

    /**
     * Retorna una colleccion de instancias log
     * @return List
     */
    @RequestMapping(method = RequestMethod.GET)
    public List<Log> index() {
        return logService.getAll();
    }

    /**
     * Busca un log por el id que recibe como parametro
     * @param id - Identificador del log a buscar
     * @return Log
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Optional<Log> index(@PathVariable("id") long id) {
        return logService.getLogById(id);
    }

    /**
     * Inserta un nuevo Log en la base de datos
     * @param log - Instancia del Log a almacenar
     * @return ResponseEntity
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity store(@RequestBody Log log) {
        if (!this.validateData(log)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        Log result = logService.save(log);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    /**
     * Valida que el Log tenga un browser y SO seteado para poder guardarlo.
     * @param log - Instancia del Log a validar
     * @return Boolean
     */
    private Boolean validateData(Log log) {
        if (log.getBrowser() == null || log.getSo() == null) {
            return false;
        }

        return true;
    }
}

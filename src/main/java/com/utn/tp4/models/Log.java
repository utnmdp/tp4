package com.utn.tp4.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Entity
@Getter
@Setter
public class Log {

    @Id
    @GeneratedValue
    private Long id;
    private String so;
    private String browser;

    public Log() {
        super();
    }

    public Log(String so, String browser) {
        super();

        this.so = so;
        this.browser = browser;
    }

}

package com.utn.tp4.repositories;

import com.utn.tp4.models.Log;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {
    //
}
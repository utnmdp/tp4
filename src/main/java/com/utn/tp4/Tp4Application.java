package com.utn.tp4;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.utn.tp4.models.Log;
import com.utn.tp4.repositories.LogRepository;

@SpringBootApplication
public class Tp4Application {

	public static void main(String[] args) {
		SpringApplication.run(Tp4Application.class, args);
	}

    /**
     * Una vez que se inicializa el p*** SpringApplication se inicializa
     * la base de datos H2 con datos fake a modo de prueba (asi no hay que cargar tantos logs)
     * Esto me lo robe de aca: https://howtodoinjava.com/spring/spring-boot/command-line-runner-interface-example/
     */
    @Bean
    public CommandLineRunner setup(LogRepository logRepository) {
		return (args) -> {
            logRepository.save(new Log("Windows 10", "Opera"));
            logRepository.save(new Log("Windows 10", "Google Chrome"));
            logRepository.save(new Log("Windows 10", "Microsoft Edge"));
			logRepository.save(new Log("Windows 10", "Mozilla Firefox"));
		};
	}

}
